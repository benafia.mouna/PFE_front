import {Injectable , Injector} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from './authService';
import {UntypedFormGroup} from '@angular/forms';

@Injectable({ providedIn: 'root'})

export class TokenInterceptorService {
  public loginForm: UntypedFormGroup;
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
     if (request.url === 'http://localhost:8090/api/v1/registration' || request.url.includes('/login') || request.url.includes('findUserEmail/')|| request.url.includes('verifPassword')|| request.url.includes('forgetpassword')|| request.url.includes('savePassword')||request.url.includes('promotion/allpromo')||request.url.includes('categorie/allcategorie')||request.url.includes('allitems')||request.url.includes('findpromo')||request.url.includes('/country/allcountry/')||request.url.includes('/city/allcity/')||request.url.includes('/modelivraison/allmode/')||request.url.includes('/modepaiement/allmode/')    ){
      
     return next.handle(request);
    }
    
    
    let str = this.auth.GetToken();
    if(str != null){
      str = str.substring(1);
      str = str.slice(0, -1);
    }
   
    console.log("ici test ===" + request.url);
        request = request.clone({
       setHeaders: {
         Authorization: `Bearer ${str}`,
       }
    });
    
     return next.handle(request);
  }
}

