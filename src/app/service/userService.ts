import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/user';
import { environment } from 'src/environments/environment';
import { TokenInterceptorService } from './tokenInterceptorService';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class UserService {


  constructor(private http: HttpClient,public auth: AuthService) { }

  public getUsers(): Observable<User[]> { 
    return this.http.get<any>(`${environment.baseurl}/user/all`);

  }

  //nofileemodifimage

  public noupdateUser(user: any  , iduser:any): Observable<User> {
    return this.http.put<User>(`${environment.baseurl}/user/noupdate/${iduser}`, user);
  } 
  public updateUser(user: any  , iduser:any): Observable<User> {
    return this.http.put<User>(`${environment.baseurl}/user/update/${iduser}`, user);
  }
  public updatePassword(id:any,newpassword:any) {
    return this.http.put(`${environment.baseurl}/user/updatepassword/${id}?newpassword=${newpassword}`, {});
  }
  public findUser(email:any) {
    return this.http.get(`${environment.baseurl}/user/findUserEmail1/${email}`);
  }
  public getCommandevendeur(id: any){
    return this.http.get(`${environment.baseurl}/commande/findbyUserId/${id}`)
}

  public deleteUser(userId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/user/delete/${userId}`);
  }
 
}
