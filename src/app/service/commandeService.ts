import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CommandeService {
  

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCommande() { 
    return this.http.get(`${environment.baseurl}/commande/all`);
  }

  public addCommande(iduser : any,idcountry : any,ids: any,commande: any) {
   // console.log('idcountry :',idcountry,' iduser :',iduser,' commande :',commande,' ids :',ids)
    return this.http.post(`${environment.baseurl}/commande/add/${iduser}/${idcountry}?ids=${ids}`, commande);
  }
  public updateCommande(commande: any , idpanier : any, id : any){
    return this.http.put(`${environment.baseurl}/commande/update/${idpanier}/${id}`, commande);
  }
  public addLivraison(livraison: any , idmodelivraison : any , idcommande : any) {
    console.log(`${environment.baseurl}/livraison/add/${idmodelivraison}/${idcommande}`)
    return this.http.post(`${environment.baseurl}/livraison/add/${idmodelivraison}/${idcommande}`, livraison);
  }
  public addPaiement(paiement: any , idcommande : any , idmodepaiement : any) {
    return this.http.post(`${environment.baseurl}/paiement/add/${idcommande}/${idmodepaiement}`, paiement);
  }

  public deleteCommande(commandeId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/commande/delete/${commandeId}`);
  }

}

