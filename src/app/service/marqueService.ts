import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class MarqueService {
 

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getMarque() { 
    return this.http.get(`${environment.baseurl}/marque/all`);
  }

  public addMarque(marque: any) {
    return this.http.post(`${environment.baseurl}/marque/add/`, marque);
    
  }

  public modifimage(marque: any, id:any){
    return this.http.put(`${environment.baseurl}/marque/modifimage/${id}`, marque);
  }
  public modifnoimage(marque: any, id:any){
    return this.http.put(`${environment.baseurl}/marque/modifnoimage/${id}`, marque);
  }
  public deleteMarque(marqueId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/marque/delete/${marqueId}`);
  }

}

