import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class PaiementService {
  

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getPaiement() { 
    return this.http.get(`${environment.baseurl}/paiement/all`);
  }

  public addPaiement(paiement: any , idcommande : any , idmodepaiement : any) {
    return this.http.post(`${environment.baseurl}/paiement/add/${idcommande}/${idmodepaiement}`, paiement);
  }
  public updatePaiement(paiement: any ,idcommande : any , idmodepaiement : any , id : any){
    return this.http.put(`${environment.baseurl}/paiement/update/${id}/${idcommande}/${idmodepaiement}`, paiement);
  }

  public deletePaiement(PaiementId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/paiement/delete/${PaiementId}`);
  }

}

