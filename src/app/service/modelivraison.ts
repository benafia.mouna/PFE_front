import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class ModeLivraisonService {
   
  constructor(private http: HttpClient,public auth: AuthService) { }

  public getModeLivraison() { 
    return this.http.get(`${environment.baseurl}/modelivraison/all`);
  }
  public getModeLivraisons() { 
    return this.http.get(`${environment.baseurl}/modelivraison/allmode`);
  }
  public addModeLivraison(modelivraison: any , idmodelivraison : any , idcommande : any) {
    return this.http.post(`${environment.baseurl}/modelivraison/add/${idmodelivraison}/${idcommande}`, modelivraison);
  }
  public updateModeLivraison(modelivraison: any ,idmodelivraison : any ,idcommande : any ,  id:any){
    return this.http.put(`${environment.baseurl}/modelivraison/update/${idmodelivraison}/${idcommande}/${id}`, modelivraison);
  }


  public deleteModeLivraison(modelivraisonId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/modelivraison/delete/${modelivraisonId}`);
  }

}

