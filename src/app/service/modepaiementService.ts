import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class ModePaiementService {
  

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getModePaiement() { 
    return this.http.get(`${environment.baseurl}/modepaiement/all`);
  }
  public getModePaiements() { 
    return this.http.get(`${environment.baseurl}/modepaiement/allmode`);
  }


  public addModePaiement(modepaiement: any) {
    return this.http.post(`${environment.baseurl}/modepaiement/add`, modepaiement);
  }
  public updateModePaiement(modepaiement: any , id:any){
    return this.http.put(`${environment.baseurl}/modepaiement/update/${id}`, modepaiement);
  }

  public deleteModePaiement(modepaiementId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/modepaiement/delete/${modepaiementId}`);
  }

}

