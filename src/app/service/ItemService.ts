import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class ItemService {
   

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getItems() { 
    return this.http.get(`${environment.baseurl}/item/allitems`);
  }
  public getItem() { 
    return this.http.get(`${environment.baseurl}/item/all`);
  }

  public addItem(item: any , idmarque : any, idpromo : any) {
    return this.http.post(`${environment.baseurl}/item/add/${idmarque}/${idpromo}`, item);
  }
  public updateItem(item: any, idmarque : any , idpromo : any , id : any){
    return this.http.put(`${environment.baseurl}/item/update/${idmarque}/${idpromo}/${id}`, item);
  }
  public noupdateItem(item: any, idmarque : any , idpromo : any , id : any){
    return this.http.put(`${environment.baseurl}/item/noupdateupdate/${idmarque}/${idpromo}/${id}`, item);
  }

  public deleteItem(itemId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/item/delete/${itemId}`);
  }

}

