import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CategorieService {
 

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCategorie() { 
    return this.http.get(`${environment.baseurl}/categorie/allcategorie`);
  }

  public addCategorie(categorie: any) {
    return this.http.post(`${environment.baseurl}/categorie/add`, categorie);
  }
  public updateCategorie(categorie: any,id:any){
    return this.http.put(`${environment.baseurl}/categorie/update/${id}`, categorie);
  }


  public deleteCategorie(categorieId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/categorie/delete/${categorieId}`);
  }

}

