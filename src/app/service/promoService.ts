import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/user';
import { environment } from 'src/environments/environment';
import { TokenInterceptorService } from './tokenInterceptorService';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class PromoService {
  

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getPromo() { 
    return this.http.get(`${environment.baseurl}/promotion/allpromo`);
  }
  public getOneePromo(id:any) {
    return this.http.get(`${environment.baseurl}/promotion/findpromo/${id}`)
  }
  public getCommandevendeur(iduser : any){
      return this.http.get(`${environment.baseurl}/promotion/findpromouser/${iduser}`)
  }
  public addPromo(promotion: any  , idsubdomaine :any , iduser : any ) {
    return this.http.post(`${environment.baseurl}/promotion/add/${idsubdomaine}/${iduser}`, promotion)
  }
  public updatePromowithFile(promotion: any , id:any){
    return this.http.put(`${environment.baseurl}/promotion/updatewithfile/${id}`, promotion)
  }
  public updatenofile(promotion: any , id:any){
    return this.http.put(`${environment.baseurl}/promotion/updatenofile/${id}`, promotion);
  }
  public updatePrix(idpromo : any , price : any){
    return this.http.put(`${environment.baseurl}/promotion/update/${idpromo}/${price}`,{})
  }
  public updatestatusconfirm(idpromo : any ){
    return this.http.put(`${environment.baseurl}/promotion/confirmstatus/${idpromo}`,{})
  }
  public updatestatusnotconfirm(idpromo : any ){
    return this.http.put(`${environment.baseurl}/promotion/notconfirmstatus/${idpromo}`,{})
  }

  public deletePromo(id: any , ids:any ) {
    return this.http.post(`${environment.baseurl}/promotion/delete/${id}?ids=${ids}`,{})
  }

}

