import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';
import { SubCategorie } from '../model/subcategorie';





@Injectable({ providedIn: 'root'})
export class SubcategorieService {


  constructor(private http: HttpClient,public auth: AuthService) { }

  public getSubcategorie():Observable<SubCategorie[]> { 
    return this.http.get<SubCategorie[]>(`${environment.baseurl}/subcategorie/all`);
  }

  public addSubCategorie(subcategorie: any , idcategorie : any) {
    return this.http.post(`${environment.baseurl}/subcategorie/add/${idcategorie}`, subcategorie);
  }
  public updateSubCategorie(subcategorie: any ,idcategorie : any ,  id : any){
    return this.http.put(`${environment.baseurl}/subcategorie/update/${idcategorie}/${id}`, subcategorie);
  }

  public deleteSubCategorie(subcategorieId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/subcategorie/delete/${subcategorieId}`);
  }

}

