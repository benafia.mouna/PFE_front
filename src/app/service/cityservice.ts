import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class CityService {
 

  constructor(private http: HttpClient,public auth: AuthService) { }

  public getCity() { 
    return this.http.get(`${environment.baseurl}/city/allcity`);
  }
}