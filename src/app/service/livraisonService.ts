import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from './authService';


@Injectable({ providedIn: 'root'})
export class LivraisonService {
   
  constructor(private http: HttpClient,public auth: AuthService) { }

  public getLivraison() { 
    return this.http.get(`${environment.baseurl}/livraison/all`);
  }

  public addLivraison(livraison: any , idmodelivraison : any , idcommande : any) {
    return this.http.post(`${environment.baseurl}/livraison/add/${idmodelivraison}/${idcommande}`, livraison);
  }
  public updateLivraison(livraison: any,idmodelivraison:any ,idcommande : any , id:any){
    return this.http.put(`${environment.baseurl}/livraison/update/${idmodelivraison}/${idcommande}/${id}`, livraison);
  }


  public deleteLivraison(livraisonId: number): Observable<void> {
    return this.http.delete<void>(`${environment.baseurl}/livraison/delete/${livraisonId}`);
  }

}

