import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LayoutComponent } from './components/layout/layout.component';
import { AuthService } from './service/authService';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './service/tokenInterceptorService';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmcompteComponent } from './components/confirmcompte/confirmcompte.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { DetailpromoComponent } from './components/detailpromo/detailpromo.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { MyaccountComponent } from './components/myaccount/myaccount.component';
import { CartComponent } from './components/cart/cart.component';
import { ListPromoComponent } from './components/list-promo/list-promo.component';
import { LogiinComponent } from './components/logiin/logiin.component';
import { RegistreeComponent } from './components/registree/registree.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgImageSliderModule } from 'ng-image-slider';
import { ListpromobycategoryComponent } from './components/listpromobycategory/listpromobycategory.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng5SliderModule } from 'ng5-slider';
import { OrderComponent } from './components/order/order.component';
import { NgxCarouselModule } from 'ngx-light-carousel';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    ConfirmcompteComponent,
    ResetpasswordComponent,
    ForgotPasswordComponent,
    DetailpromoComponent,
    ContactUsComponent,
    WishlistComponent,
    MyaccountComponent,

    CartComponent,
    ListPromoComponent,
    LogiinComponent,
    RegistreeComponent,
    ListpromobycategoryComponent,
    OrderComponent
  
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    AppRoutingModule,
    Ng2SearchPipeModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgImageSliderModule,
    NgxPaginationModule,
    Ng5SliderModule,
    NgxCarouselModule


  ],
  providers: [AuthService  ,
    {
      provide : HTTP_INTERCEPTORS,
      useClass : TokenInterceptorService  , multi:true

    },
    
    

  ],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
