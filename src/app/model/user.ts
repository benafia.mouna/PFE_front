import {Role} from './role';

export class User {
  id?: number;
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  enabled?: boolean;
  acceptTerms?: boolean;
  phone?: string;
  type?: string;
  mat_fiscale ?: string;
  cin ?: string;
  adresse ?: string;

  PasswordResetToken?: string;
  roles?: Role[];



}
