import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListpromobycategoryComponent } from './listpromobycategory.component';

describe('ListpromobycategoryComponent', () => {
  let component: ListpromobycategoryComponent;
  let fixture: ComponentFixture<ListpromobycategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListpromobycategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListpromobycategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
