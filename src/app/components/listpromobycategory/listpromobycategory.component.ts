import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Options } from 'ng5-slider';
import { PromoService } from 'src/app/service/promoService';

@Component({
  selector: 'app-listpromobycategory',
  templateUrl: './listpromobycategory.component.html',
  styleUrls: ['./listpromobycategory.component.css']
})
export class ListpromobycategoryComponent implements OnInit {

  promo:any
  term:any
  p:number=1;
  priceSelection=""
  minValue = 0;
  maxValue = 1000;
  options: Options = {
    floor: 0,
    ceil: 1000
  };
  id=this.activeroute.snapshot.params["id"]
  constructor( private promoservice : PromoService,private activeroute:ActivatedRoute) { }

  ngOnInit(): void {

    this.getpromo()
  }
  getpromo(){
  
    this.promoservice.getPromo().subscribe((res:any)=>{
      console.log(res)
      this.promo=res.filter((el:any)=>el.status=="Confirmée" && el.subcategorie.categorie.id==this.id)
      console.log("promo",this.promo)
    })
  }

  changePrice() {
    console.log('Price change', this.priceSelection);
    let event = this.priceSelection
    this.promoservice.getPromo().subscribe((res:any)=>{
      this.promo=res
      if (event !== undefined) {
        const ListproduitByPrice = this.promo.filter((elemt: any) => elemt.price >= event[0] && elemt.price <= event[1]);
        this.promo = ListproduitByPrice;
        console.log('filter by price', this.promo)
      }
    })
  }
}
