import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from 'src/app/service/cart.service';
import { CategorieService } from 'src/app/service/categorieService';
import { ItemService } from 'src/app/service/ItemService';
import { PromoService } from 'src/app/service/promoService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  promo: any
  listcategorie: any
  title = 'NgxCarouselLibrary'
  public innerWidth: any
  id = this.activeroute.snapshot.params['id']
  qte_user: number = 1
  items = [] as any
  listitem:any
  products3: any=[{
    title: 'Black Widgets111',
    regularPrice: '100.00',
    image: "assets\img\slider-1.jpg",
  }]
  options1: any
  options2: any
  options3: any

  constructor( private activeroute: ActivatedRoute,private promoservice: PromoService, private categorieService: CategorieService,private cartservice:CartService,private itemService:ItemService) {

    this.options1 = {
      animation: {
        animationClass: 'transition',
        animationTime: 500,
      },
      swipe: {
        swipeable: true,
        swipeVelocity: .004,
      },
      drag: {
        draggable: true,
        dragMany: true,
      },
      arrows: true,
      infinite: true,
      autoplay: {
        enabled: true,
        direction: 'right',
        delay: 5000,
        stopOnHover: true,
        speed: 6000,
      },
      breakpoints: [
        {
          width: 768,
          number: 1,
        },
        {
          width: 991,
          number: 3,
        },
        {
          width: 9999,
          number: 4,
        },
      ],
    }
    this.options2 = {
      animation: {
        animationClass: 'transition', // done
        animationTime: 500,
      },
      swipe: {
        swipeable: true, // done
        swipeVelocity: .004, // done - check amount
      },
      drag: {
        draggable: true, // done
        dragMany: true, // todo
      },
      autoplay: {
        enabled: true,
        direction: 'right',
        delay: 5000,
        stopOnHover: true,
        speed: 6000,
      },
      arrows: true,
      infinite: false,
      breakpoints: [
        {
          width: 768,
          number: 1,
        },
        {
          width: 991,
          number: 3,
        },
        {
          width: 9999,
          number: 3,
        },
      ],
    }
    this.options3 = {
      animation: {
        animationClass: 'transition', // done
        animationTime: 500,
      },
      swipe: {
        swipeable: true, // done
        swipeVelocity: .004, // done - check amount
      },
      drag: {
        draggable: true, // done
        dragMany: true, // todo
      },
      autoplay: {
        enabled: true,
        direction: 'right',
        delay: 5000,
        stopOnHover: true,
        speed: 6000,
      },
      arrows: true,
      infinite: true,
      breakpoints: [
        {
          width: 768,
          number: 1,
        },
        {
          width: 991,
          number: 3,
        },
        {
          width: 9999,
          number: 4,
        },
      ],
    }

 this.products3 = []

    this.products3.push({
      title: 'Black Widgets111',
    
      regularPrice: '100.00',
      image: "assets\img\slider-1.jpg",
    }) 
    this.products3.push({
     
    
   
      image: "assets\img\slider-4.jpg",
    }) 





  }

  ngOnInit(): void {
    console.log("promo")
    this.getallcategorie()
    this.getpromo()
    this.getallitems()
    this.getOneepromo()
  }
  getallcategorie() {

    this.categorieService.getCategorie().subscribe(res => {
      console.log(res)
      this.listcategorie = res
      console.log("list categorie", this.listcategorie)
    })

  }
  getOneepromo() {
    this.promoservice.getOneePromo(this.id).subscribe(res => {
      this.promo = res

      console.log(" promo mouna", this.promo)

    })
  }



  async getallitems() {

    await this.itemService.getItems().subscribe((res: any) => {

      this.listitem = res
        .filter((el: any) => el.promo.id == this.id)
      console.log("itemssssssssssssss", this.listitem)

    })
  }

  getpromo() {

    this.promoservice.getPromo().subscribe((res: any) => {
      console.log(res)
      this.promo = res
        .filter((el: any) => el.status == "Confirmée")
      console.log("promo", this.promo)
    })
  }
  addToCart() {
    console.log("msssssg", this.promo,this.qte_user)
     // if (!this.cartservice.itemInCart(this.promo)) {
        this.promo.qte = this.qte_user
        this.cartservice.addToCart(this.promo); //add item in cart
        this.items = this.cartservice.getItems();
  
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Promotion Added',
          showConfirmButton: false,
          timer: 2000
        })
        location.reload()
      //}
  }


}
