import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/service/cart.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {
  totalprixpromo = 0
  promo = [] as any
  items = [] as any
  qte_user: number = 1

  constructor(private cartservice: CartService) { }

  ngOnInit(): void {
    this.cartservice.loadCart()
    this.promo = this.cartservice.getItems()
    console.log("items", this.promo)
    this.gettotal()
  

  }
  removepromo(i: any) {
    this.cartservice.removeItem(i);
    this.promo = this.cartservice.getItems()
    window.location.reload
  }
  gettotal() {
    let totalprix = 0;
    this.promo.forEach((element: any) => {
      totalprix += Number(element.price) * Number(element.qte);

    });
    this.totalprixpromo = totalprix;
    console.log("total", this.totalprixpromo)
    return totalprix;

  }
  addToCart() {
    console.log("msssssg", this.promo,this.qte_user)
     // if (!this.cartservice.itemInCart(this.promo)) {
        this.promo.qte = this.qte_user
        this.cartservice.addToCart(this.promo); //add item in cart
        this.items = this.cartservice.getItems();
  
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Promotion Added',
          showConfirmButton: false,
          timer: 1500
        })
        location.reload()
      //}
      //}
  }
 

}
