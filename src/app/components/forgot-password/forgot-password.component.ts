import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/authService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public  forgotForm : FormGroup;
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {
this.createLoginForm();
   }
   createLoginForm() {
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

   sendemail() {
    
    console.log(this.forgotForm.value.email)
    this.authService.forgetpassword(this.forgotForm.value.email).subscribe( data=>{
     if(data == "False"){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
        footer: '<a href="">Email not found!!</a>'
      })
     }else{
      Swal.fire(
        'Good job!',
        'Mail is sent!',
        'success'
      )
     }
   
    })
  }


  ngOnInit(): void {
  }



}