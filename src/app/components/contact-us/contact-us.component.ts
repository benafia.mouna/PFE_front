import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Mail } from 'src/app/model/mail';
import { User } from 'src/app/model/user';
import { MailService } from 'src/app/service/mailService';
import { UserService } from 'src/app/service/userService';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  addForm: FormGroup;
  user: User;
  email = localStorage.getItem("email")
  constructor(private mailService: MailService,private form: FormBuilder,private userservice: UserService) { }

  ngOnInit() {
    this.addForm = this.form.group({
      subject: ['', Validators.required],
      content: ['', Validators.required],


    });
    this.findUserByEmail()
}
  Envoyermail() {
    const m: Mail = {
      subject: this.addForm.value.subject,
      content: this.addForm.value.content,
      // to: localStorage.getItem("email"),
      from: "genova.angel@hotmail.com"
    };
    //console.log('7eta lennna cv ca marche tres bien', m.to, m.subject, m.content)
    this.mailService.sendmail(m).subscribe((res: any) => {
      console.log("response of send mail : ", res)

    });
  }
  
  findUserByEmail() {
    this.userservice.findUser(this.email).subscribe(res => {
      this.user = res
    })

  }
}
