import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { PromoService } from 'src/app/service/promoService';
import { UserService } from 'src/app/service/userService';
import Validation from 'src/helper/match';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {
  testRole = localStorage.getItem("role");
  listuser: any;
  submitted = false;
  user: User;
  order:any
  fileToUpload:Array<File>=[];
  userupdate : FormGroup;
  changepassword : FormGroup;
  url:any;
  email= localStorage.getItem("email")
  listpromouser:any
    constructor(private promoService:PromoService,private userservice : UserService, private form:FormBuilder, private router : Router, private userService:UserService) { }
  
    ngOnInit() {
       
  
       this.findUserByEmail()
  
       this.userupdate = this.form.group({
        firstName: ['',Validators.required],
        lastName: ['', Validators.required],
        email: ['', Validators.required],
        phone: ['', Validators.required],
        adresse: ['', Validators.required],
        newpassword: [''],
     
      })
      this.changepassword = this.form.group({
        currentPassword:['',Validators.required],
        password:['',Validators.required],
        confirmPassword:['',Validators.required]
      }, {
        validators: Validation.match('password', 'confirmPassword')
  
      });
      
  
    }
  
  
   
    get f() { return this.changepassword.controls; }
    
   
  
 
  
  
  updatepassword(){
    this.submitted = true;
      if (this.changepassword.invalid) {
        return;
      }
    this.userservice.updatePassword(this.user.id,this.changepassword.value.newpassword).subscribe(( res:any )=>{
      console.log(res)
      Swal.fire({
     
        icon: 'success',
        title: 'Password updated !',
        showConfirmButton: false,
        timer: 1500
      })
    })
  }
  
  upadateuser() {
    let formdata = new FormData()
    formdata.append("firstName", this.userupdate.value.firstName)
    formdata.append("lastName", this.userupdate.value.lastName)
    formdata.append("email", this.userupdate.value.email)
    formdata.append("phone", this.userupdate.value.phone)
    formdata.append("adresse", this.userupdate.value.adresse)
    this.userservice.noupdateUser(formdata, this.user.id).subscribe((res: any) => {
      console.log(res)
      Swal.fire({
        icon: 'success',
        title: 'Updated successfully !',
        showConfirmButton: false,
        timer: 2000
      })
        
    })
    location.reload();
  }



  
    logout() {

      localStorage.clear()
      this.router.navigate(["/login"])
    }
    findUserByEmail() {
      this.userService.findUser(this.email).subscribe((res: any) => {
          this.user = res
        console.log(" of user ", res)
        console.log("role of user", res.id)
      
        this.userservice.getCommandevendeur(res.id).subscribe(res => {
          this.order = res

                    })
      })
    }
}
