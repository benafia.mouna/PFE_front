import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/authService';
import Validation from 'src/helper/match';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-logiin',
  templateUrl: './logiin.component.html',
  styleUrls: ['./logiin.component.css']
})
export class LogiinComponent implements OnInit {
  loginForm :FormGroup;
  responsedata: any;
  user = new User();
  constructor(private formBuilder : FormBuilder , private authService : AuthService, private router : Router) { }

  ngOnInit(): void {
    this.createLoginForm();
    localStorage.clear();
  }
     
  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {

    this.user.email = this.loginForm.value.username;
    this.user.password = this.loginForm.value.password;
   

    if (this.loginForm.valid) {
      this.authService.VerificationMDP(this.user).subscribe(voila => {
        console.log('reponse de verification de mot de passe : ', voila)
        if (voila === true) {
          this.authService.findUserByEmail(this.loginForm.value.username).subscribe((res:any) => {
           
            if (res === true) {
              this.authService.login(this.loginForm.value).subscribe(
                result => {
                     if (result != null) {
                    console.log(this.loginForm.value.username);
                    this.responsedata = result;
                    console.log("result : ",result);
                    localStorage.setItem('token',  result);
                    localStorage.setItem("email",this.loginForm.value.username)
                    
                    localStorage.setItem("state","1")
                    
                    this.authService.findUser(this.loginForm.value.username).subscribe((res:any)=>{
                     
                    
                                          
                      console.log('ici res aaaaa : ',res.roles[0].name)
                      if(res.roles[0].name != "CLIENT"){
                        Swal.fire({
                          icon: 'error',
                          title: 'Oops...',
                          text: 'You must have an account !',
                          
                        })
                        this.router.navigate(['/registree']);
                      }else{
                        this.router.navigate(['/']);
                      }
                      
                      
                    })
                   
                  }
                },
              );
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'your account is not verified!',
                
              })
              
            }

          }
          );
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Invalid email or password !',
          
          })
           this.router.navigate(['/auth/login']);
        }
      })

    }
    
   
  }


}
