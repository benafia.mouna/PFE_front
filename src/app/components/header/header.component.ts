import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { CartService } from 'src/app/service/cart.service';
import { UserService } from 'src/app/service/userService';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
promo=[] as any
mail= localStorage.getItem("email")
test = false
Username : any
user: User
  constructor(private cartservice:CartService , private userService : UserService) { }
  

  ngOnInit(): void {
    this.cartservice.loadCart()
    this.promo=this.cartservice.getItems()
    this.VerifConex()
    console.log('le test est ici : ',this.test)
    
    

  }
VerifConex(){
  if (this.mail !=null){
    this.test = true
    this.userService.findUser(this.mail).subscribe(res=>{
     
      this.user = res
      this.Username =  this.user.firstName
    })
  }
  
}



}
