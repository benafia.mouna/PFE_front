import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';
import { CategorieService } from 'src/app/service/categorieService';
import { PromoService } from 'src/app/service/promoService';

@Component({
  selector: 'app-list-promo',
  templateUrl: './list-promo.component.html',
  styleUrls: ['./list-promo.component.css']
})
export class ListPromoComponent implements OnInit {
  listcategorie: any
  term: any
  priceSelection = ""
  minValue = 0;
  maxValue = 1000;
  options: Options = {
    floor: 0,
    ceil: 1000
  };
  promo: any
  p: number = 1;
  constructor(private categorieService: CategorieService, private promoservice: PromoService) { }

  ngOnInit(): void {
    this.getallcategorie()
    this.getpromo()
  }
  getallcategorie() {

    this.categorieService.getCategorie().subscribe(res => {
      console.log(res)
      this.listcategorie = res
      console.log("list categorie", this.listcategorie)
    })

  }

  getpromo() {

    this.promoservice.getPromo().subscribe((res: any) => {
      console.log("refo of prmos",res)
      const now = new Date();

      this.promo = res
        .filter((el: any) => el.status == "Confirmée" && new Date(el.date_fin)>now)
      console.log("promo", this.promo)
    })
  }
  changePrice() {
    console.log('Price change', this.priceSelection);
    let event = this.priceSelection
    this.promoservice.getPromo().subscribe((res: any) => {
      this.promo = res
      if (event !== undefined) {
        const ListproduitByPrice = this.promo.filter((elemt: any) => elemt.price >= event[0] && elemt.price <= event[1]);
        this.promo = ListproduitByPrice;
        console.log('filter by price', this.promo)
      }
    })
  }

}
