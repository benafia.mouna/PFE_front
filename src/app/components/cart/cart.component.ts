import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/model/user';
import { CartService } from 'src/app/service/cart.service';
import { LivraisonService } from 'src/app/service/livraisonService';
import { ModeLivraisonService } from 'src/app/service/modelivraison';
import { ModePaiementService } from 'src/app/service/modepaiementService';
import { UserService } from 'src/app/service/userService';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  totalprixpromo = 0
  promo = [] as any



  constructor(private cartservice: CartService) { }

  ngOnInit(): void {
    this.cartservice.loadCart()
    this.promo = this.cartservice.getItems()
    console.log("items", this.promo)
    this.gettotal()
  

  }
  removepromo(i: any) {
    this.cartservice.removeItem(i);
    this.promo = this.cartservice.getItems()
    window.location.reload
  }
  gettotal() {
    let totalprix = 0;
    this.promo.forEach((element: any) => {
      totalprix += Number(element.price) * Number(element.qte);
      console.log('total prix :',totalprix)

    });
    this.totalprixpromo = totalprix;
    console.log("total", this.totalprixpromo)
    return totalprix;

  }
  testf(){
    console.log('ca maaaaaaaaaaaaarche !')
  }
 

}
