import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from 'src/app/service/cart.service';
import { CategorieService } from 'src/app/service/categorieService';
import { ItemService } from 'src/app/service/ItemService';
import { PromoService } from 'src/app/service/promoService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detailpromo',
  templateUrl: './detailpromo.component.html',
  styleUrls: ['./detailpromo.component.css']
})
export class DetailpromoComponent implements OnInit {


  title = 'NgxCarouselLibrary'
  public innerWidth: any

  products3: any = [{
    title: 'Black Widgets111',
    regularPrice: '100.00',
    image: "assets\img\slider-1.jpg",
  }]
  options1: any
  options2: any
  options3: any



  id = this.activeroute.snapshot.params['id']
  promo: any
  listitem: any = []
  indexs = [] as any
  result: any
  k: any = 0;
  item: any
  url_ = "http://127.0.0.1:8887/"
  qte_user: number = 1
  listcategorie: any
  items = [] as any
  imageObject = [{
  }];

  constructor(private promoService: PromoService, private activeroute: ActivatedRoute,
    private itemService: ItemService, private categorieService: CategorieService, private cartservice: CartService) {

    this.options3 = {
      animation: {
        animationClass: 'transition', // done
        animationTime: 500,
      },
      swipe: {
        swipeable: true, // done
        swipeVelocity: .004, // done - check amount
      },
      drag: {
        draggable: true, // done
        dragMany: true, // todo
      },
      autoplay: {
        enabled: true,
        direction: 'right',
        delay: 5000,
        stopOnHover: true,
        speed: 6000,
      },
      arrows: true,
      infinite: true,
      breakpoints: [
        {
          width: 768,
          number: 1,
        },
        {
          width: 991,
          number: 3,
        },
        {
          width: 9999,
          number: 4,
        },
      ],
    }



    this.products3 = []


    this.products3.push({
      title: 'Black Widgets2222222222222222',

      regularPrice: '100.00',
      image: "assets\img\slider-2.jpg",
    })

    this.products3.push({
      title: 'Black Widgets333333333',
      regularPrice: '100.00',
      image: "assets\img\slider-3.jpg",

    })
  }

  ngOnInit(): void {

    this.getOneepromo()
    this.getallitems()
    this.getallcategorie()

  }

  getOneepromo() {
    this.promoService.getOneePromo(this.id).subscribe(res => {
      this.promo = res

      console.log(" promo mouna", this.promo)

    })
  }



  async getallitems() {

    await this.itemService.getItems().subscribe((res: any) => {

      this.listitem = res
        .filter((el: any) => el.promo.id == this.id)
      console.log("itemssssssssssssss", this.listitem)

    })
  }

  getallcategorie() {

    this.categorieService.getCategorie().subscribe(res => {
      console.log(res)
      this.listcategorie = res
      console.log("list categorie", this.listcategorie)
    })

  }

  plus() {
    this.qte_user++
  }
  munis() {
    this.qte_user--;
  }
  addToCart() {
    console.log("msssssg", this.promo,this.qte_user)
     // if (!this.cartservice.itemInCart(this.promo)) {
        this.promo.qte = this.qte_user
        this.cartservice.addToCart(this.promo); //add item in cart
        this.items = this.cartservice.getItems();
  
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Promotion Added',
          showConfirmButton: false,
          timer: 2000
        })
        location.reload()
      //}
  }
}