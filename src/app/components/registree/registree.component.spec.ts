import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistreeComponent } from './registree.component';

describe('RegistreeComponent', () => {
  let component: RegistreeComponent;
  let fixture: ComponentFixture<RegistreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
