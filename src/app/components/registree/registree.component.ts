import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/authService';
import Validation from 'src/helper/match';

@Component({
  selector: 'app-registree',
  templateUrl: './registree.component.html',
  styleUrls: ['./registree.component.css']
})
export class RegistreeComponent implements OnInit {
  registerForm : FormGroup;
  showPassword: boolean = false;
  submitted = false;
  constructor( private formBuilder : FormBuilder , private authService : AuthService , private router : Router) { }

  ngOnInit(): void {
    this.createRegisterForm();
    localStorage.clear();
  }

  createRegisterForm() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone:['',Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      // acceptTerms: [false, Validators.requiredTrue],
      email: ['', [Validators.required, Validators.email]],
   
    }, {
      validators: Validation.match('password', 'confirmPassword')

    });
  }
  get f() { return this.registerForm.controls; }

  register() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    
    let formdata= new FormData()
    formdata.append("firstName",this.registerForm.value.firstName)
    formdata.append("password",this.registerForm.value.password)
    formdata.append("lastName",this.registerForm.value.lastName)
    formdata.append("phone",this.registerForm.value.phone)
    formdata.append("email",this.registerForm.value.email)
    formdata.append("roles","CLIENT");
   

    this.authService.register(formdata).subscribe((res:any)=>{
     
      this.router.navigateByUrl("/confirm")
    });
 

  }
  showHidePassword() {
    this.showPassword = !this.showPassword;
  }
}
