import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';
import { CartService } from 'src/app/service/cart.service';
import { CityService } from 'src/app/service/cityservice';
import { CommandeService } from 'src/app/service/commandeService';
import { CountryService } from 'src/app/service/countryservice';
import { ModeLivraisonService } from 'src/app/service/modelivraison';
import { ModePaiementService } from 'src/app/service/modepaiementService';
import { PromoService } from 'src/app/service/promoService';
import { UserService } from 'src/app/service/userService';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  items = [] as any
  city: any;
  country: any;
  form: FormGroup
  form1: FormGroup
  form2: FormGroup
  ids: any = []
  listmodepaie:any = []
  totalprixpromo: any=""
  user: User;
  mail:any
  ///Username:any
 // test = false;
  listmodeliv: any =[]
  promo=[];
  radioSelected: any;
  discount=656;
  email = localStorage.getItem("email")
  date = new Date().toISOString().split('T')[0].toString()
  enum_details = [
    {name: 'pardeep'},
    {name: 'Jain'},
    {name: 'Angular'},
    {name: 'Anbbbbbbgular'},
  ]
  constructor(private router : Router,private userService:UserService,private modelivraisonservice : ModeLivraisonService,private modepaiementService : ModePaiementService,private promoService: PromoService,private cityService: CityService, private countryService: CountryService, private cartservice: CartService, private formbuilder: FormBuilder, private userservice: UserService, private CommandeService: CommandeService) { }

  ngOnInit(): void {

    this.items = this.cartservice.getItems()
    this.createForm()
    this.findUserByEmail()
    this.getallcountry()
    this.gettotal()
    this.getallmodelivraison()
    this.getallmodepaiement()
    //this.VerifConex()

    console.log('wawawawawawaawaw :',this.form.value.modelivraison)
  }
  createForm() {
    this.form = this.formbuilder.group({
      date: ['', Validators.required],
      adresse: ['', Validators.required],
      total: ['', Validators.required],
      idcountry: ['', Validators.required],
      id_modelivraison:['', Validators.required],
      id_modepaiement:['', Validators.required],
      // item_order: ['', Validators.required], 

    });
    this.form1 = this.formbuilder.group({
      date:this.date,
      
      ///id_modepaiement:['', Validators.required],
      id_commande:"",
      id_modelivraison:['', Validators.required],
      Status:"encours",
      

    });
    this.form2 = this.formbuilder.group({
      date:this.date,
      
      ///id_modepaiement:['', Validators.required],
      id_commande:"",
      id_modepaiement:['', Validators.required],
      Status:"encours",
      

    });
    
   
  }
 
  async gettotal() {


    console.log("total before ", this.totalprixpromo)

   
    let totalprix = 0;
    await this.items.forEach((element: any) => { totalprix += Number(element.price) * Number(element.qte); 
    //console.log('id promo : ',element.id)
    
    });
    let x=[]
    x= this.items
    for (let i = 0; this.items.length> i; i++) {
      this.ids[i]=this.items[i].id
    }
    console.log('list promo : ',this.ids)
    this.totalprixpromo = totalprix;
    
    

  }
  findUserByEmail() {
  
    this.userservice.findUser(this.email).subscribe(res => {
      this.user = res
      
    })
  }
  addCommande() {

    // if(localStorage.getItem("token")) {

    //   Swal.fire({
    //     title: "Votre Prix n est pas en promo ! essaye un autre prix !"// ici
    //   })
    // this.form.patchValue({
    //   date: this.date,


    // })} else {

    //   Swal.fire({
    //     title: "Votre Prix n est pas en promo essaye un autre prix !"// ici
    //   })
    // }
   

    this.form.patchValue({
         date: this.date,
   
       })
    console.log("country", this.form.value.total = this.totalprixpromo )
    this.CommandeService.addCommande(this.user.id, this.form.value.idcountry, this.ids, this.form.value).subscribe((res: any) => {
      
      this.form1.value.id_commande= res.id
      console.log('1: ',this.form.value.id_modepaiement,'2: ',this.form1.value.id_modepaiement,'3: ',this.form2.value.id_modepaiement,'3: ',this.form.value.modepaiement)
      this.CommandeService.addLivraison(this.form1.value,this.form.value.id_modelivraison,this.form1.value.id_commande).subscribe()
      this.CommandeService.addPaiement(this.form2.value,res.id,this.form.value.id_modepaiement ).subscribe()
      Swal.fire({
     
        icon: 'success',
        title: 'Order Added',
        showConfirmButton: false,
        timer: 1500
      })
    });
    this.router.navigate(['/']);

  }
  // onAddLivraison(ida : any) {
    
    
  //   this.CommandeService.addLivraison(this.addFormLivraison.value,this.addFormLivraison.value.id_modelivraison, this.addFormLivraison.value.id_commande).subscribe(
  //     (reponse: any) => {
  //       console.log(reponse);
       
  //     },
  //   );
  // }

  
  getallcity(e: any) {
    console.log(e.target.value)
    this.cityService.getCity().subscribe((res: any) => {
      this.city = res
        .filter((el: any) => el.country1.id == e.target.value)
      console.log("city", this.city)
    })
  }
  getallcountry() {
    this.countryService.getCountry().subscribe(res => {
      this.country = res;
      console.log("country", this.country)
    })
  }
  getallmodelivraison() {

    this.modelivraisonservice.getModeLivraisons().subscribe(res => {
      console.log(res)
      this.listmodeliv = res
      console.log("list mode livraison", this.listmodeliv)
    })
  }
  getallmodepaiement() {
    this.modepaiementService.getModePaiements().subscribe(res => {
      console.log(res)
      this.listmodepaie = res
      console.log("list mode paiement", this.listmodepaie)
    })
  }

}
