import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { ConfirmcompteComponent } from './components/confirmcompte/confirmcompte.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { DetailpromoComponent } from './components/detailpromo/detailpromo.component';
import { EmailvalidComponent } from './components/emailvalid/emailvalid.component';
import { ErrorComponent } from './components/error/error.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ListPromoComponent } from './components/list-promo/list-promo.component';
import { ListpromobycategoryComponent } from './components/listpromobycategory/listpromobycategory.component';
import { LogiinComponent } from './components/logiin/logiin.component';
import { MyaccountComponent } from './components/myaccount/myaccount.component';
import { OrderComponent } from './components/order/order.component';
import { RegistreeComponent } from './components/registree/registree.component';
import { WishlistComponent } from './components/wishlist/wishlist.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [

  {path:'',component:HomeComponent,children:[
    {path:'',component:LayoutComponent},
    {path:'detailpromo/:id',component:DetailpromoComponent},
    {path:'contact',component:ContactUsComponent},
    {path:'wishlist',component:WishlistComponent},
    {path:'myaccount',component:MyaccountComponent,canActivate:[AuthGuard]},
    {path:'order',component:OrderComponent,canActivate:[AuthGuard]},
    {path:'cart',component:CartComponent},
    {path:'promotion',component:ListPromoComponent},
    {path:'listpromo/:id',component:ListpromobycategoryComponent},
   


  ]},
  {path:'login',component:LogiinComponent},
  {path:'registree',component:RegistreeComponent},
  {path:'confirm',component:ConfirmcompteComponent},
  {path:'forget',component:ForgotPasswordComponent},
  {path:'valid',component:EmailvalidComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
